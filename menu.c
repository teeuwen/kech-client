/*
 *
 * kech-client
 * menu.c
 *
 * Copyright (C) 2018 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string.h>
#include <unistd.h>

#include "kbp.h"
#include "kech.h"

enum {
	MODE_STANDBY,
	MODE_NORMAL,
	MODE_ACCOUNTS,
	MODE_IBAN,
	MODE_AMOUNT,
	MODE_PIN
};

enum {
	MENU_NONE,
	MENU_STANDBY,
	MENU_PINENTRY,
	MENU_LOGIN_MESSAGE,
	MENU_MAIN_MESSAGE,
	MENU_MAIN,
	MENU_WITHDRAW,
	MENU_WITHDRAW_AMOUNT,
	MENU_WITHDRAW_AMOUNT_CUSTOM,
	MENU_DEPOSIT,
	MENU_TRANSFER,
	MENU_TRANSFER_DEST,
	MENU_TRANSFER_AMOUNT,
	MENU_ACCOUNTS,
	MENU_PINCHANGE
};

struct button {
	const char	*text;
	int		menu;
	void		(*handler) ();
	int		arg;
};

struct menu {
	const int			mode;
	const char			*message;
	char				*message_alt;
	void				(*handler) ();
	struct kbp_reply_account	accounts[BUTTON_MAX];
	struct button			buttons_left[BUTTON_MAX],
					buttons_right[BUTTON_MAX];
};

static const char btnkey_left[] = { 'A', 'B', 'C', 'D' };
static const char btnkey_right[] = { '1', '4', '7', '*' };

static volatile int		menu = MENU_STANDBY;
static volatile bool		querying;

static struct button		*pressed_btn;
static int			pressed_acc = -1;
static struct kbp_reply_account	selected_src_acc, selected_dest_acc;

static void login();
static void logout();
static void withdraw(int amount);
static void withdraw_amount(struct kbp_reply_account *acc);
static void transfer(int amount);
static void transfer_dest(struct kbp_reply_account *acc);
static void transfer_amount(struct kbp_reply_account *acc);
static void pin_change();

static struct menu menus[] = {
	[MENU_STANDBY] = {
#if 0
		.mode = MODE_STANDBY,
		.message = "Please present your card",
#else
		.mode = MODE_NORMAL,
		.message = "Please present your card",
		.buttons_right = {
			{ "fake card", MENU_PINENTRY, NULL, 0 }
		}
#endif
	},
	[MENU_PINENTRY] = {
		.mode = MODE_PIN,
		.message = "Please enter your PIN",
		.handler = &login,
		.buttons_right = {
			{ "Accept", 0, &login, 0 }
		}
	},
	[MENU_LOGIN_MESSAGE] = {
		.mode = MODE_NORMAL,
		.buttons_right = {
			{ "Okay", MENU_STANDBY, NULL, 0 }
		}
	},
	[MENU_MAIN_MESSAGE] = {
		.mode = MODE_NORMAL,
		.buttons_left = {
			{ "Back", MENU_MAIN, NULL, 0 }
		},
		.buttons_right = {
			{ "Log out", 0, &logout, 0 }
		}
	},
	[MENU_MAIN] = {
		.mode = MODE_NORMAL,
		.buttons_left = {
			{ "Withdraw", MENU_WITHDRAW, NULL, 0 },
			{ "Deposit", MENU_DEPOSIT, NULL, 0 },
			{ "Transfer", MENU_TRANSFER, NULL, 0 }
		},
		.buttons_right = {
			{ "Accounts", MENU_ACCOUNTS, NULL, 0 },
			{ "Change PIN", MENU_PINCHANGE, NULL, 0 },
			{ "Log out", 0, &logout, 0 }
		}
	},
	[MENU_WITHDRAW] = {
		.mode = MODE_ACCOUNTS,
		.message = "Please select the account to withdraw from",
		.handler = &withdraw_amount,
		.buttons_left = {
			{ "Cancel", MENU_MAIN, NULL, 0 }
		}
	},
	[MENU_WITHDRAW_AMOUNT] = {
		.mode = MODE_NORMAL,
		.message = "Please select the desired amount",
		.buttons_left = {
			{ "EUR 10", 0, &withdraw, 10 },
			{ "EUR 20", 0, &withdraw, 20 },
			{ "EUR 50", 0, &withdraw, 50 },
			{ "Cancel", MENU_MAIN, NULL, 0 }
		},
		.buttons_right = {
			{ "EUR 100", 0, &withdraw, 100 },
			{ "EUR 200", 0, &withdraw, 200 },
			{ "EUR 500", 0, &withdraw, 500 },
			{ "Custom", MENU_WITHDRAW_AMOUNT_CUSTOM, NULL, 0 }
		}
	},
	[MENU_WITHDRAW_AMOUNT_CUSTOM] = {
		.mode = MODE_AMOUNT,
		.message = "Please enter the desired amount in EUR",
		.handler = &withdraw,
		.buttons_left = {
			{ "Cancel", MENU_MAIN, NULL, 0 }
		},
		.buttons_right = {
			{ "Accept", 0, &withdraw, -1 }
		}
	},
	[MENU_DEPOSIT] = {
		.mode = MODE_AMOUNT,
		.message = "TODO",
		.buttons_left = {
			{ "Cancel", MENU_MAIN, NULL, 0 }
		}
	},
	[MENU_TRANSFER] = {
		.mode = MODE_ACCOUNTS,
		.message = "Please select the source account",
		.handler = &transfer_dest,
		.buttons_left = {
			{ "Cancel", MENU_MAIN, NULL, 0 }
		}
	},
	[MENU_TRANSFER_DEST] = {
		.mode = MODE_ACCOUNTS,
		.message = "Please select the destination account",
		.handler = &transfer_amount,
		.buttons_left = {
			{ "Cancel", MENU_MAIN, NULL, 0 }
		}
	},
	[MENU_TRANSFER_AMOUNT] = {
		.mode = MODE_AMOUNT,
		.message = "Please enter the amount to transfer",
		.handler = &transfer,
		.buttons_left = {
			{ "Cancel", MENU_MAIN, NULL, 0 }
		},
		.buttons_right = {
			{ "Accept", 0, &transfer, -1 }
		}
	},
	[MENU_ACCOUNTS] = {
		.mode = MODE_ACCOUNTS,
		.buttons_left = {
			{ "Back", MENU_MAIN, NULL, 0 }
		}
	},
	[MENU_PINCHANGE] = {
		.mode = MODE_PIN,
		.message = "Please enter your new PIN",
		.handler = &pin_change,
		.buttons_left = {
			{ "Cancel", MENU_MAIN, NULL, 0 }
		},
		.buttons_right = {
			{ "Accept", 0, &pin_change, 0 }
		}
	}
};

static void _login()
{
	struct kbp_reply *rep;
	char *msg = NULL;

	querying = 0;

	rep = (struct kbp_reply *) qbuf;
	switch (rep->status) {
	case KBP_S_OK:
		switch ((uint8_t) *(qbuf + sizeof(struct kbp_reply))) {
		case KBP_L_GRANTED:
			menu = MENU_MAIN;
			break;
		case KBP_L_DENIED:
			msg = "Invalid PIN, please try again";
			break;
		case KBP_L_BLOCKED:
			menu = MENU_LOGIN_MESSAGE;
			msg = "This card is blocked, "
					"please contact Kech Bank support";
			break;
		}
		break;
	case KBP_S_INVALID:
	default:
		menu = MENU_STANDBY;
		msg = "This ATM is out of service";
		break;
	}

	if (msg) {
		if (!(menus[menu].message_alt = malloc(strlen(msg) + 1))) {
			fprintf(stderr, "out of memory\n");
			return;
		}
		strcpy(menus[menu].message_alt, msg);
	}

	draw_force();
}

static void login()
{
	struct kbp_request_login *l;

	if (querying)
		return;

	if (!bufi) {
		free(menus[menu].message_alt);
		menus[menu].message_alt = NULL;
		return;
	}

	if (!(l = malloc(sizeof(struct kbp_request_login)))) {
		fprintf(stderr, "out of memory\n");
		return;
	}

	/* TODO Actually read from a card */
	l->uid[0] = 0x00;
	l->uid[1] = 0x00;
	l->uid[2] = 0x78;
	l->uid[3] = 0x56;
	l->uid[4] = 0x34;
	l->uid[5] = 0x12;
	strncpy(l->pin, buf, bufi);
	l->pin[bufi] = '\0';

	if ((querying = query(KBP_T_LOGIN, (char *) l,
			sizeof(struct kbp_request_login))))
		signal(SIGUSR1, &_login);
}

static void logout()
{
	if (querying)
		return;

	menu = MENU_STANDBY;

	if (query(KBP_T_LOGOUT, NULL, 0))
		signal(SIGUSR1, SIG_IGN);
}

static void _withdraw()
{
	struct kbp_reply *rep;
	char *msg = NULL;

	querying = 0;

	rep = (struct kbp_reply *) qbuf;
	switch (rep->status) {
	case KBP_S_OK:
		msg = "Please take out your money";
		break;
	case KBP_S_FAIL:
		msg = "Insufficient balance";
		break;
	case KBP_S_TIMEOUT:
		logout();
		break;
	case KBP_S_INVALID:
	default:
		menu = MENU_MAIN;
		break;
	}

	if (msg) {
		if (!(menus[menu].message_alt = malloc(strlen(msg) + 1))) {
			fprintf(stderr, "out of memory\n");
			return;
		}
		strcpy(menus[menu].message_alt, msg);
	}

	draw_force();
}

static void withdraw(int amount)
{
	struct kbp_request_transfer *t;

	if (querying)
		return;

	if (!*selected_src_acc.iban)
		return;

	if (!(t = malloc(sizeof(struct kbp_request_transfer)))) {
		fprintf(stderr, "out of memory\n");
		return;
	}

	strncpy(t->iban_src, selected_src_acc.iban, KBP_IBAN_MAX);
	t->iban_dest[0] = '\0';
	if (amount < 0)
		t->amount = strtoll(buf, NULL, 10) * 100;
	else
		t->amount = amount * 100;

	if ((querying = query(KBP_T_TRANSFER, (char *) t,
			sizeof(struct kbp_request_transfer)))) {
		signal(SIGUSR1, &_withdraw);
		menu = MENU_MAIN_MESSAGE;
	}
}

static void withdraw_amount(struct kbp_reply_account *acc)
{
	memcpy(&selected_src_acc, acc, sizeof(struct kbp_reply_account));
	menu = MENU_WITHDRAW_AMOUNT;
}

static void _transfer()
{
	struct kbp_reply *rep;
	char *msg = NULL;

	querying = 0;

	rep = (struct kbp_reply *) qbuf;
	switch (rep->status) {
	case KBP_S_OK:
		msg = "Transfer completed";
		break;
	case KBP_S_FAIL:
		msg = "Insufficient balance";
		break;
	case KBP_S_TIMEOUT:
		logout();
		break;
	case KBP_S_INVALID:
	default:
		menu = MENU_MAIN;
		break;
	}

	if (msg) {
		if (!(menus[menu].message_alt = malloc(strlen(msg) + 1))) {
			fprintf(stderr, "out of memory\n");
			return;
		}
		strcpy(menus[menu].message_alt, msg);
	}

	draw_force();
}

static void transfer(int amount)
{
	struct kbp_request_transfer *t;

	if (querying)
		return;

	if (!*selected_src_acc.iban || !*selected_dest_acc.iban)
		return;

	if (!(t = malloc(sizeof(struct kbp_request_transfer)))) {
		fprintf(stderr, "out of memory\n");
		return;
	}

	strncpy(t->iban_src, selected_src_acc.iban, KBP_IBAN_MAX);
	strncpy(t->iban_dest, selected_dest_acc.iban, KBP_IBAN_MAX);
	if (amount < 0)
		t->amount = strtoll(buf, NULL, 10) * 100;
	else
		t->amount = amount * 100;

	if ((querying = query(KBP_T_TRANSFER, (char *) t,
			sizeof(struct kbp_request_transfer)))) {
		signal(SIGUSR1, &_transfer);
		menu = MENU_MAIN_MESSAGE;
	}
}

static void transfer_dest(struct kbp_reply_account *acc)
{
	memcpy(&selected_src_acc, acc, sizeof(struct kbp_reply_account));
	menu = MENU_TRANSFER_DEST;
}

static void transfer_amount(struct kbp_reply_account *acc)
{
	memcpy(&selected_dest_acc, acc, sizeof(struct kbp_reply_account));
	menu = MENU_TRANSFER_AMOUNT;
}

static void _pin_change()
{
	struct kbp_reply *rep;
	char *msg = NULL;

	querying = 0;
	menu = MENU_MAIN_MESSAGE;

	rep = (struct kbp_reply *) qbuf;
	switch (rep->status) {
	case KBP_S_OK:
		msg = "Your PIN has successfully been changed";
		break;
	case KBP_S_FAIL:
		msg = "A PIN must contain 4 or more numbers";
		break;
	case KBP_S_TIMEOUT:
		logout();
		break;
	case KBP_S_INVALID:
	default:
		menu = MENU_MAIN;
		break;
	}

	if (msg) {
		if (!(menus[menu].message_alt = malloc(strlen(msg) + 1))) {
			fprintf(stderr, "out of memory\n");
			return;
		}
		strcpy(menus[menu].message_alt, msg);
	}

	draw_force();
}

static void pin_change()
{
	char *p;

	if (querying)
		return;

	if (!bufi) {
		free(menus[menu].message_alt);
		menus[menu].message_alt = NULL;
		return;
	}

	if (!(p = calloc(1, KBP_PIN_MAX + 1))) {
		fprintf(stderr, "out of memory\n");
		return;
	}

	strncpy(p, buf, bufi);
	p[bufi] = '\0';

	if ((querying = query(KBP_T_PIN_UPDATE, (char *) p, KBP_PIN_MAX + 1)))
		signal(SIGUSR1, &_pin_change);
}

static void _accounts()
{
	struct kbp_reply *rep;
	struct kbp_reply_account *acc;
	int i, n;

	querying = 0;

	rep = (struct kbp_reply *) qbuf;
	switch (rep->status) {
	case KBP_S_OK:
		acc = (struct kbp_reply_account *)
				(qbuf + sizeof(struct kbp_reply));
		n = rep->length / sizeof(struct kbp_reply_account);

		/* FIXME This is kind of a problem */
		if (n > BUTTON_MAX)
			return;

		for (i = 0; i < n; i++)
			memcpy(&menus[menu].accounts[i], &acc[i],
					sizeof(struct kbp_reply_account));
		break;
	case KBP_S_TIMEOUT:
		logout();
		break;
	case KBP_S_INVALID:
	default:
		menu = MENU_MAIN;
		break;
	}

	draw_force();
}

static void _wait_card()
{
	querying = 0;

	menu = MENU_PINENTRY;
	draw_force();
}

/* FIXME Better */
static void *wait_card(void *_self)
{
	char buf[128];
	pthread_t *self = _self;

	fgets(buf, 128, stdin);

	pthread_kill(*self, SIGUSR1);
	pthread_exit(NULL);
}

static void draw_standby(void)
{
	pthread_t t;

	if (querying)
		return;
	querying = 1;

	if (pthread_create(&t, NULL, wait_card, (void *) pthread_self()))
		fprintf(stderr, "unable to create thread\n");
	else
		signal(SIGUSR1, &_wait_card);
}

static void draw_accounts(void)
{
	char *text = NULL;
	int i, n, o;

	/*
	 * FIXME? Infinite loop if user has no accounts, but should that even be
	 * possible?
	 */

	/* First retrieve all accounts */
	if (!*menus[menu].accounts[0].iban) {
		if ((querying = query(KBP_T_ACCOUNTS, NULL, 0)))
			signal(SIGUSR1, &_accounts);
		return;
	}

	for (n = 0; *menus[menu].accounts[n].iban && n < BUTTON_MAX; n++);
	if (menu == MENU_TRANSFER_DEST)
		n--;

	for (i = 0, o = 0; i < n + o; i++) {
		if (menu == MENU_TRANSFER_DEST && strcmp(selected_src_acc.iban,
				menus[menu].accounts[i].iban) == 0) {
			o = 1;
		} else {
			if (!(text = realloc(text, 3 +
					strlen(menus[menu].accounts[i].iban) +
					1)))
				return;

			sprintf(text, "%d) %s", 1 + i - o,
					menus[menu].accounts[i].iban);

			draw_account(i - o, n, (i == pressed_acc), text,
					menus[menu].accounts[i].type,
					menus[menu].accounts[i].balance);
		}
	}

	free(text);
}

static void draw_buttons(struct button *btns, bool side)
{
	char *text = NULL, k;
	int i, n;

	if (!btns)
		return;

	for (n = 0; btns[n].text && n < BUTTON_MAX; n++);

	/* Can't be bigger than 4 due to keypad limitations */
	if (n > 4)
		return;

	for (i = 0; i < n; i++) {
		if (!(text = realloc(text, 3 + strlen(btns[i].text) + 1)))
			return;

		if (menus[menu].mode != MODE_NORMAL || n <= 2)
			k = side ? '*' : '#';
		else
			k = side ? btnkey_left[i] : btnkey_right[i];

		sprintf(text, "%c) %s", k, btns[i].text);

		draw_button(i, n, side, (&btns[i] == pressed_btn), text);
	}

	free(text);
}

static struct button *button_check(struct button *btns, bool side,
		int x, int y)
{
	int _x, _y, i, n;

	for (n = 0; btns[n].text && n < BUTTON_MAX; n++);

	/* Calculate button positions and check if coordinates align */
	for (i = 0; i < n; i++) {
		_x = side ? w - BUTTON_PADDING - BUTTON_WIDTH :
			BUTTON_PADDING;
		_y = BANNER_HEIGHT + (h - BANNER_HEIGHT) / 2 -
				(n * (BUTTON_HEIGHT + BUTTON_PADDING)) / 2 +
				i * (BUTTON_HEIGHT + BUTTON_PADDING);

		if (x >= _x && x <= _x + BUTTON_WIDTH &&
				y >= _y && y <= _y + BUTTON_HEIGHT)
			return &btns[i];
	}

	/*
	 * FIXME Maybe simply perform these calculations statically you
	 * dickhead?
	 */

	return NULL;
}

static int account_check(int x, int y)
{
	int _x, _y, i, n;

	for (n = 0; *menus[menu].accounts[n].iban && n < BUTTON_MAX; n++);
	if (menu == MENU_TRANSFER_DEST)
		n--;

	/* Calculate button positions and check if coordinates align */
	for (i = 0; i < n; i++) {
		_x = 2 * BUTTON_PADDING + BUTTON_WIDTH;
		_y = BANNER_HEIGHT + (h - BANNER_HEIGHT) / 2 -
				(n * (BUTTON_HEIGHT + BUTTON_PADDING)) / 2 +
				i * (BUTTON_HEIGHT + BUTTON_PADDING);

		if (x >= _x && x <= w - _x &&
				y >= _y && y <= _y + BUTTON_HEIGHT)
			return i;
	}

	/*
	 * FIXME Maybe simply perform these calculations statically you double
	 * dickhead?
	 */

	return -1;
}

void buttons_check(bool press, int x, int y)
{
	struct button *btn = NULL;
	int acc = -1, menu_old;

	if (!(btn = button_check(menus[menu].buttons_left, 0, x, y)) &&
			(acc = account_check(x, y)) < 0 && !(btn =
			button_check(menus[menu].buttons_right, 1, x, y))) {
		pressed_btn = NULL;
		pressed_acc = -1;
		return;
	}

	if (btn) {
		if (press) {
			pressed_btn = btn;
		} else if (pressed_btn) {
			free(menus[menu].message_alt);
			menus[menu].message_alt = NULL;
			menu_old = menu;

			if (pressed_btn->menu)
				menu = pressed_btn->menu;
			else if (pressed_btn->handler)
				pressed_btn->handler(pressed_btn->arg);

			clear_keybuf();
			memset(menus[menu].accounts, 0, BUTTON_MAX *
					sizeof(struct kbp_reply_account));
			pressed_btn = NULL;
		}
	} else if (menu != MENU_ACCOUNTS) {
		if (press) {
			pressed_acc = acc;
		} else if (pressed_acc >= 0) {
			free(menus[menu].message_alt);
			menus[menu].message_alt = NULL;
			menu_old = menu;

			if (menus[menu].handler)
				menus[menu].handler(&menus[menu]
						.accounts[pressed_acc]);

			clear_keybuf();
			memset(menus[menu_old].accounts, 0, BUTTON_MAX *
					sizeof(struct kbp_reply_account));
			pressed_acc = -1;
		}
	}
}

/* Check if currently focused menu needs user input from the keypad */
int need_input(int n)
{
	if (menus[menu].buttons_left || menus[menu].buttons_right)
		return 1;

	if (menus[menu].mode == MODE_PIN) {
		if (n > PIN_LENGTH - 1)
			return 0;
		else
			return 1;
	} else if (menus[menu].mode == MODE_AMOUNT) {
		if (n > AMOUNT_LENGTH - 1)
			return 0;
		else
			return 1;
	}

	return -1;
}

void draw_menu(void)
{
	draw_background();
	draw_banner();

	/* Print "Please wait..." if a query is running */
	if (querying && menus[menu].mode != MODE_STANDBY) {
		draw_text(0, h - (h - BANNER_HEIGHT) / 5, w, 48,
				"Please wait...", 1, ALIGN_CENTER, 32);
		return;
	}

	if (menus[menu].message_alt)
		draw_text(0, h - (h - BANNER_HEIGHT) / 5, w, 48,
				menus[menu].message_alt, 1, ALIGN_CENTER, 32);
	else
		draw_text(0, h - (h - BANNER_HEIGHT) / 5, w, 48,
				menus[menu].message, 1, ALIGN_CENTER, 32);

	if (bufi > KEY_ENTER) {
		/* Call a handler if 'Return' is pressed */
		free(menus[menu].message_alt);
		menus[menu].message_alt = NULL;

		bufi -= KEY_ENTER;

		if (menus[menu].handler)
			menus[menu].handler(-1);

		clear_keybuf();
	} else {
		/* Draw all control elements */
		if (menus[menu].mode == MODE_STANDBY)
			draw_standby();
		else if (menus[menu].mode == MODE_ACCOUNTS)
			draw_accounts();
		else if (menus[menu].mode == MODE_IBAN)
			draw_ibanenter();
		else if (menus[menu].mode == MODE_AMOUNT)
			draw_amount();
		else if (menus[menu].mode == MODE_PIN)
			draw_pin();

		if (menus[menu].mode != MODE_STANDBY) {
			draw_buttons(menus[menu].buttons_left, 0);
			draw_buttons(menus[menu].buttons_right, 1);
		}
	}
}
