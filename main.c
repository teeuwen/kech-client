/*
 *
 * kech-client
 * main.c
 *
 * Copyright (C) 2018 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>

#include <errno.h>
#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#if SSLSOCK
#  include <openssl/err.h>
#  include <openssl/ssl.h>

#  define READ(b, n)	SSL_read(ssl, (b), (n))
#  define WRITE(b, n)	SSL_write(ssl, (b), (n))
#else
#  include <limits.h>

#  define READ(b, n)	read(sock, (b), (n))
#  define WRITE(b, n)	write(sock, (b), (n))
#endif

#include "kbp.h"
#include "kech.h"

struct query {
	struct kbp_request	req;
	char			*buf;
	pthread_t		self;
};

static char host[HOST_NAME_MAX];
static char port[6];
#if SSLSOCK
static char *ca, *cert, *key;

static SSL_CTX *ctx;
static SSL *ssl;
#endif
static int sock;

static pthread_t	thread;
char			qbuf[sizeof(struct kbp_reply) + KBP_LENGTH_MAX];

/* FIXME Send KBP_S_INVALID when an error occurs */
static void *_query(void *_q)
{
	struct query *q = _q;
	struct kbp_reply *rep;
	/* int res; */

	/* FIXME Check if the socket is still up */
	/* if (getsockopt(sock, SOL_SOCKET, SO_ERROR, &res, NULL) < 0) {
		fprintf(stderr, "%s: unable to get socket options\n", ip);
		goto ret;
	}
	if (res != 0) {
		fprintf(stderr, "%s: connection lost\n", ip);
		goto ret;
	} */

	/* Send the request header */
	if (WRITE(&q->req, sizeof(q->req)) <= 0) {
		fprintf(stderr, "%s: header write error\n", host);
		goto ret;
	}

	/* Send the request data */
	if (q->req.length && WRITE(q->buf, q->req.length) <= 0) {
		fprintf(stderr, "%s: write error\n", host);
		goto ret;
	}

	/* Wait for a reply from the server */
	if (READ(qbuf, sizeof(struct kbp_reply)) <= 0) {
		fprintf(stderr, "%s: header read error\n", host);
		goto ret;
	}
	rep = (struct kbp_reply *) qbuf;

	if (rep->magic != KBP_MAGIC || rep->length > KBP_LENGTH_MAX) {
		fprintf(stderr, "%s: invalid reply\n", host);
		goto ret;
	}
	if (rep->version != KBP_VERSION) {
		fprintf(stderr, "%s: KBP version mismatch (got %u want %u)\n",
				host, rep->version, KBP_VERSION);
		goto ret;
	}

	/* Read the reply data if available */
	if (rep->length && READ((qbuf + sizeof(struct kbp_reply)),
			rep->length) <= 0) {
		fprintf(stderr, "%s: read error\n", host);
		goto ret;
	}

ret:
	/* Signal the main thread to draw the result on screen */
	pthread_kill(q->self, SIGUSR1);
	free(q->buf);
	free(_q);
	pthread_exit(NULL);
}

int query(uint8_t type, char *data, int32_t n)
{
	struct query *q;

	if (!(q = malloc(sizeof(struct query)))) {
		fprintf(stderr, "out of memory\n");
		free(data);
		return 0;
	}

	q->req.magic = KBP_MAGIC;
	q->req.version= KBP_VERSION;
	q->req.type = type;
	q->req.length = n;
	q->buf = data;
	q->self = pthread_self();

	if (pthread_create(&thread, NULL, &_query, q)) {
		fprintf(stderr, "unable to create thread\n");
		free(data);
		return 0;
	}

	return 1;
}

static int init(void)
{
	struct addrinfo *addr = NULL, hints;
	struct in6_addr server;
#if SSLSOCK
	const SSL_METHOD *met;

	/* Initialize OpenSSL */
	if (!(met = TLS_client_method()))
		goto err;
	if (!(ctx = SSL_CTX_new(met)))
		goto err;

	/* Load CA */
	if (access(ca, F_OK) < 0) {
		fprintf(stderr, "%s: %s\n", ca, strerror(errno));
		goto err;
	}
	if (!SSL_CTX_load_verify_locations(ctx, ca, NULL)) {
		fprintf(stderr, "unable to load CA: %s\n", ca);
		goto err;
	}

	/* Load certificate and private key files */
	if (access(cert, F_OK) < 0) {
		fprintf(stderr, "%s: %s\n", cert, strerror(errno));
		goto err;
	}
	if (SSL_CTX_use_certificate_file(ctx, cert, SSL_FILETYPE_PEM) != 1)
		goto err;

	if (access(key, F_OK) < 0) {
		fprintf(stderr, "%s: %s\n", key, strerror(errno));
		goto err;
	}
	if (SSL_CTX_use_PrivateKey_file(ctx, key, SSL_FILETYPE_PEM) != 1)
		goto err;

	if (!SSL_CTX_check_private_key(ctx))
		goto err;
#endif

	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_flags = AI_NUMERICSERV | AI_NUMERICHOST;
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;

	/* Check where host is an IPv4 address, IPv6 address or hostname */
	if (inet_pton(AF_INET, host, &server) > 0)
		hints.ai_family = AF_INET;
	else if (inet_pton(AF_INET6, host, &server) > 0)
		hints.ai_family = AF_INET6;
	else
		hints.ai_flags &= ~AI_NUMERICHOST;

	/* Resolve the host */
	if (getaddrinfo(host, port, &hints, &addr) != 0) {
		fprintf(stderr, "unable to resolve host %s\n", host);
		goto err;
	}

	/* Create the socket */
	if ((sock = socket(addr->ai_family, addr->ai_socktype,
			addr->ai_protocol)) < 0) {
		fprintf(stderr, "unable to create socket: %s\n",
				strerror(errno));
		goto err;
	}

	/* Wait for the server */
	if (connect(sock, addr->ai_addr, addr->ai_addrlen) < 0) {
		fprintf(stderr, "unable to connect to server @ %s:%s\n",
				host, port);
		goto err;
	}

#if SSLSOCK
	/* Setup an SSL/TLS connection */
	if (!(ssl = SSL_new(ctx))) {
		fprintf(stderr, "unable to allocate SSL structure\n");
		goto err;
	}
	SSL_set_fd(ssl, sock);

	if (SSL_connect(ssl) <= 0) {
		fprintf(stderr, "SSL handshake error\n");
		goto err;
	}

	/* Get and verify the server certificate */
	SSL_CTX_set_verify(ctx, SSL_VERIFY_PEER, NULL);
	SSL_CTX_set_verify_depth(ctx, 1);
	if (!SSL_get_peer_certificate(ssl)) {
		fprintf(stderr, "server failed to present certificate\n");
		goto err;
	}
	if (SSL_get_verify_result(ssl) != X509_V_OK) {
		fprintf(stderr, "certificate verfication failed\n");
		goto err;
	}
#endif

	printf("connected to server %s:%s\n", host, port);

	return 0;

err:
	freeaddrinfo(addr);
	close(sock);
#if SSLSOCK
	ERR_print_errors_fp(stderr);
	SSL_free(ssl);
	ssl = NULL;
	if (ctx)
		SSL_CTX_free(ctx);
	ctx = NULL;
#endif

	return -1;
}

static void usage(char *prog)
{
	printf("Usage: %s [OPTION...]\n\n%s", prog,
			"  -i HOST              host to connect to\n"
			"  -p PORT NUMBER       port number to connect to\n"
			"  -C FILE              CA file to use\n"
			"  -c FILE              certificate file to use\n"
			"  -k FILE              private key file to use\n"
			"  -h                   show this help message\n"
			);
}

int main(int argc, char **argv)
{
	int c;

	/* Parse arguments */
	while ((c = getopt(argc, argv, "i:p:"
#if SSLSOCK
			"C:c:k:"
#endif
			"h")) != -1) {
		switch (c) {
		/* Host */
		case 'i':
			strncpy(host, optarg, HOST_NAME_MAX);
			break;
		/* Port number */
		case 'p':
			strncpy(port, optarg, 6);
			break;
#if SSLSOCK
		/* CA file path */
		case 'C':
			if (!(ca = malloc(strlen(optarg) + 1)))
				goto err;
			strcpy(ca, optarg);
			break;
		/* Certificate file path */
		case 'c':
			if (!(cert = malloc(strlen(optarg) + 1)))
				goto err;
			strcpy(cert, optarg);
			break;
		/* Key file path */
		case 'k':
			if (!(key = malloc(strlen(optarg) + 1)))
				goto err;
			strcpy(key, optarg);
			break;
#endif
		/* Usage */
		case 'h':
			usage(argv[0]);
#if SSLSOCK
			free(cert);
#endif

			return 0;
		case '?':
		default:
			usage(argv[0]);
			goto err;
		}
	}

#if SSLSOCK
	if (!ca) {
		fprintf(stderr, "please specify a CA file\n");
		usage(argv[0]);
		goto err;
	} else if (!cert) {
		fprintf(stderr, "please specify a certificate file\n");
		usage(argv[0]);
		goto err;
	} else if (!key) {
		fprintf(stderr, "please specify a private key file\n");
		usage(argv[0]);
		goto err;
	}
#endif

	if (!*host)
		strcpy(host, "127.0.0.1");
	if (!port[0])
		sprintf(port, "%d", KBP_PORT);

	if (init() < 0)
		goto err;
	draw();

	printf("shutting down...\n");

#if SSLSOCK
	SSL_shutdown(ssl);
	SSL_free(ssl);
	SSL_CTX_free(ctx);
	free(cert);
#endif
	close(sock);

	return 0;

err:
#if SSLSOCK
	free(cert);
#endif

	return 1;
}
