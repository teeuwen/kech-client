/*
 *
 * kech-client
 * draw.c
 *
 * Copyright (C) 2018 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include <fontconfig/fontconfig.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL2_gfxPrimitives.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include <limits.h>
#include <string.h>

#include "kbp.h"
#include "kech.h"

extern const void	*_binary_logo_png_start, *_binary_logo_png_end;

static SDL_Window	*win;
static SDL_Renderer	*ren;
static SDL_Cursor	*cur;
static SDL_Surface	*logo_sur;
uint16_t		w, h;

static TTF_Font		*font;
static const char	*font_name = "DejaVu Sans";
static char		font_path[PATH_MAX];
static int		font_size;

char			buf[KEY_BUFFER];
unsigned char		bufi;

static void die(const char *errstr, ...)
{
	va_list ap;

	va_start(ap, errstr);
	vfprintf(stderr, errstr, ap);
	va_end(ap);

	draw_fini();
	pthread_exit(NULL);
	exit(1);
}

void clear_keybuf(void)
{
	bufi = 0;
	buf[0] = '\0';
}

void draw_banner(void)
{
	SDL_Texture *ft;
	SDL_Rect dest = {
		w / 2 - BANNER_WIDTH / 2, 0, BANNER_WIDTH, BANNER_HEIGHT
	};

	ft = SDL_CreateTextureFromSurface(ren, logo_sur);
	SDL_RenderCopy(ren, ft, NULL, &dest);

	SDL_DestroyTexture(ft);
}

void draw_background(void)
{
	boxRGBA(ren, 0, 0, w, h, 0xF5, 0xF5, 0xF5, 0xFF);
}

void draw_text(int x, int y, int _w, int _h, const char *text, bool dark,
		char align, int size)
{
	SDL_Color color;
	SDL_Rect dest;
	SDL_Surface *s;
	SDL_Texture *ft;

	if (!text)
		return;

	/*
	 * Reopen the font if size doesn't match
	 * FIXME Probably not very efficient
	 */
	if (font_size != size) {
		TTF_CloseFont(font);

		if (!(font = TTF_OpenFont(font_path, size)))
			die("unable to open font: %s\n", font_path);
	}

	/* Set attributes and write text to surface */
	if (dark)
		color = (SDL_Color) { 0x00, 0x00, 0x00, 0xFF };
	else
		color = (SDL_Color) { 0xFF, 0xFF, 0xFF, 0xDE };
	s = TTF_RenderUTF8_Blended(font, text, color);

	/* Calculate the position */
	if (TTF_SizeUTF8(font, text, &dest.w, &dest.h) < 0)
		die("unable to calculate font dimensions\n");

	switch (align) {
	case ALIGN_LEFT:
		dest.x = x + BUTTON_PADDING;
		break;
	case ALIGN_CENTER:
		dest.x = x + _w / 2 - dest.w / 2;
		break;
	case ALIGN_RIGHT:
		dest.x = x + _w - dest.w - BUTTON_PADDING;
		break;
	}
	dest.y = y + _h / 2 - dest.h / 2;

	/* Copy text surface to window */
	ft = SDL_CreateTextureFromSurface(ren, s);
	SDL_RenderCopy(ren, ft, NULL, &dest);

	SDL_FreeSurface(s);
	SDL_DestroyTexture(ft);
}

void draw_button(int n, int t, bool side, bool pressed, const char *text)
{
	int x, y;

	x = side ? w - BUTTON_PADDING - BUTTON_WIDTH : BUTTON_PADDING;
	y = BANNER_HEIGHT + (h - BANNER_HEIGHT) / 2 -
			(t * (BUTTON_HEIGHT + BUTTON_PADDING)) / 2 +
			n * (BUTTON_HEIGHT + BUTTON_PADDING);

	if (pressed)
		boxRGBA(ren, x, y, x + BUTTON_WIDTH, y + BUTTON_HEIGHT,
				0x21, 0x21, 0x21, 0xFF);
	else
		boxRGBA(ren, x, y, x + BUTTON_WIDTH, y + BUTTON_HEIGHT,
				0x42, 0x42, 0x42, 0xFF);

	draw_text(x, y, BUTTON_WIDTH, BUTTON_HEIGHT, text, 0, ALIGN_CENTER, 16);
}

void draw_account(int n, int t, bool pressed, char *iban, uint8_t type,
		int64_t balance)
{
	int x, y, _w;
	char buf[32];

	x = 2 * BUTTON_PADDING + BUTTON_WIDTH;
	y = BANNER_HEIGHT + (h - BANNER_HEIGHT) / 2 -
			(t * (BUTTON_HEIGHT + BUTTON_PADDING)) / 2 +
			n * (BUTTON_HEIGHT + BUTTON_PADDING);
	_w = w - x * 2;

	if (pressed)
		boxRGBA(ren, x, y, x + _w, y + BUTTON_HEIGHT,
				0x21, 0x21, 0x21, 0xFF);
	else
		boxRGBA(ren, x, y, x + _w, y + BUTTON_HEIGHT,
				0x42, 0x42, 0x42, 0xFF);

	draw_text(x, y, _w, BUTTON_HEIGHT, iban, 0, ALIGN_LEFT, 28);

	switch (type) {
	case KBP_A_CHECKING:
		strcpy(buf, "Checking account");
		break;
	case KBP_A_SAVINGS:
		strcpy(buf, "Savings account");
		break;
	default:
		strcpy(buf, "");
		break;
	}
	draw_text(x, y, _w, BUTTON_HEIGHT, buf, 0, ALIGN_CENTER, 24);

	sprintf(buf, "EUR %.2f", (double) balance / 100);
	draw_text(x, y, _w, BUTTON_HEIGHT, buf, 0, ALIGN_RIGHT, 32);
}

void draw_ibanenter(void)
{
	/* TODO */
	draw_text(0, BANNER_HEIGHT, TEXTBOX_WIDTH, TEXTBOX_HEIGHT, "TODO", 1,
			ALIGN_CENTER, 64);
}

void draw_amount(void)
{
	int x, y;

	x = w / 2 - TEXTBOX_WIDTH / 2;
	y = BANNER_HEIGHT + (h - BANNER_HEIGHT) / 2 - TEXTBOX_HEIGHT / 2;

	boxRGBA(ren, x, y, x + TEXTBOX_WIDTH, y + TEXTBOX_HEIGHT,
			0x42, 0x42, 0x42, 0xFF);

	draw_text(x, y, TEXTBOX_WIDTH, TEXTBOX_HEIGHT, buf, 0, ALIGN_CENTER,
			64);
}

void draw_pin(void)
{
	int x, y, _w, _h, i;

	x = w / 2 - PINBOX_WIDTH / 2;
	y = BANNER_HEIGHT + (h - BANNER_HEIGHT) / 2 - TEXTBOX_HEIGHT / 2;

	boxRGBA(ren, x, y, x + PINBOX_WIDTH, y + TEXTBOX_HEIGHT,
			0x42, 0x42, 0x42, 0xFF);

	for (i = 0; i < bufi; i++) {
		_w = x + (BULLET_RADIUS / 2 + BULLET_PADDING) +
			PINBOX_WIDTH / 2 -
			((BULLET_RADIUS * 2 + BULLET_PADDING) * bufi) / 2 +
			i * (BULLET_RADIUS * 2 + BULLET_PADDING);
		_h = y + TEXTBOX_HEIGHT / 2;

		filledCircleRGBA(ren, _w, _h, BULLET_RADIUS, 0xFF, 0xFF, 0xFF,
				0xDE);
		/* FIXME This is kinda shitty */
		aacircleRGBA(ren, _w, _h, BULLET_RADIUS, 0xFF, 0xFF, 0xFF,
				0xDE);
	}
}

static void text_update(SDL_Keycode s)
{
	int res;

	if (bufi > KEY_BUFFER)
		return;

	/* First check if current menu needs keyboard input */
	if ((res = need_input(bufi)) < 0)
		return;

	/* Check if backspace or return is pressed */
	if (bufi && (s == SDLK_BACKSPACE || s == SDLK_KP_PERIOD)) {
		buf[--bufi] = '\0';
		return;
	} else if (bufi && (s == SDLK_RETURN || s == SDLK_KP_ENTER)) {
		bufi += KEY_ENTER;
		return;
	}

	if (!res)
		return;

	/* Ugh... */
	switch (s) {
	case SDLK_0:
	case SDLK_KP_0:
		buf[bufi++] = '0';
		break;
	case SDLK_1:
	case SDLK_KP_1:
		buf[bufi++] = '1';
		break;
	case SDLK_2:
	case SDLK_KP_2:
		buf[bufi++] = '2';
		break;
	case SDLK_3:
	case SDLK_KP_3:
		buf[bufi++] = '3';
		break;
	case SDLK_4:
	case SDLK_KP_4:
		buf[bufi++] = '4';
		break;
	case SDLK_5:
	case SDLK_KP_5:
		buf[bufi++] = '5';
		break;
	case SDLK_6:
	case SDLK_KP_6:
		buf[bufi++] = '6';
		break;
	case SDLK_7:
	case SDLK_KP_7:
		buf[bufi++] = '7';
		break;
	case SDLK_8:
	case SDLK_KP_8:
		buf[bufi++] = '8';
		break;
	case SDLK_9:
	case SDLK_KP_9:
		buf[bufi++] = '9';
		break;
	}

	buf[bufi] = '\0';
}

/* Just send any ol' unused event */
void draw_force(void)
{
	SDL_Event e;
	e.type = SDL_WINDOWEVENT;
	SDL_PushEvent(&e);
}

static void font_init(void)
{
	FcConfig *fc;
	FcPattern *fp, *f;
	FcResult res;
	FcChar8 *path;

	/* Initialize fontconfig */
	if (!(fc = FcInitLoadConfigAndFonts()))
		die("unable to init fontconfig\n");

	/* Set up the pattern */
	if (!(fp = FcNameParse((const FcChar8 *) font_name)))
		die("unable to parse font name");
	FcConfigSubstitute(fc, fp, FcMatchPattern);
	FcDefaultSubstitute(fp);

	/* Find the font */
	if (!(f = FcFontMatch(fc, fp, &res)))
		die("unable to find font %s\n", font_name);

	/* Store the path */
	if (FcPatternGetString(f, FC_FILE, 0, (FcChar8 **) &path) !=
			FcResultMatch)
		die("unable to find font %s\n", font_name);
	strncpy(font_path, (char *) path, PATH_MAX);

	/* Intialize SDL2 font rendering */
	if (TTF_Init() < 0)
		die("unable to intialize SDL2 TTF library\n");

	FcPatternDestroy(f);
	FcPatternDestroy(fp);
	FcConfigDestroy(fc);
}

static void draw_init(void)
{
	SDL_DisplayMode dm;
	Uint8 cur_data[2] = { 0 };
	SDL_RWops *logo;
	void *logo_start, *logo_end;

	if (SDL_Init(SDL_INIT_VIDEO) < 0)
		die("unable to intialize SDL\n");

	/* Create a window */
	if (!(win = SDL_CreateWindow("kech", 0, 0, 640, 480,
			SDL_WINDOW_SHOWN | SDL_WINDOW_FULLSCREEN_DESKTOP)))
		die("unable to create window\n");

	/* Create a renderer */
	if (!(ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_SOFTWARE)))
		die("unable to create renderer\n");
	SDL_RenderClear(ren);

	SDL_GetDesktopDisplayMode(0, &dm);
	w = dm.w;
	h = dm.h;

	/* Hide the cursor */
#if 0
	if (!(cur = SDL_CreateCursor(cur_data, cur_data, 8, 8, 4, 4)))
		die("unable to create cursor\n");
	SDL_SetCursor(cur);
#endif

	/* Initialize SDL2 image loading */
	if (!IMG_Init(IMG_INIT_PNG))
		die("unable to intialize SDL2 image library\n");

	logo_start = &_binary_logo_png_start;
	logo_end = &_binary_logo_png_end;
	logo = SDL_RWFromConstMem(logo_start, logo_end - logo_start);

	if (!(logo_sur = IMG_Load_RW(logo, 0)))
		die("unable to load logo.png: %s\n", IMG_GetError());

	font_init();
}

void draw_fini(void)
{
	TTF_CloseFont(font);

	IMG_Quit();

	SDL_FreeCursor(cur);
	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
	SDL_Quit();
}

void draw(void)
{
	SDL_Event e;

	draw_init();

	for (;;) {
		if (SDL_WaitEvent(&e)) {
			/* These are unused and only yield a high cpu usage */
			if (e.type == SDL_MOUSEMOTION)
				continue;

			switch (e.type) {
			case SDL_FINGERDOWN:
			case SDL_FINGERUP:
				buttons_check(e.type == SDL_FINGERDOWN,
						e.tfinger.x * w,
						e.tfinger.y * h);

				break;
			case SDL_MOUSEBUTTONDOWN:
			case SDL_MOUSEBUTTONUP:
				if (e.button.button != SDL_BUTTON_LEFT)
					break;

				buttons_check(e.type == SDL_MOUSEBUTTONDOWN,
						e.button.x, e.button.y);

				break;
			case SDL_KEYDOWN:
				text_update(e.key.keysym.sym);
				break;
			case SDL_QUIT:
				goto ret;
			}
		}

		/* Draw the UI */
		draw_menu();

		SDL_RenderPresent(ren);
	}

ret:
	draw_fini();
}
