#
# Makefile
#

CC		:= gcc
LD		:= ld

PKGLIST		= fontconfig sdl2 SDL2_gfx SDL2_image SDL2_ttf openssl

MAKEFLAGS	:= -s

CFLAGS		:= -Wall -Wextra -Wpedantic -std=c99 -D_XOPEN_SOURCE=700 `pkg-config --cflags $(PKGLIST)` -g -O0 #-DSSLSOCK #-s -Os
LDFLAGS		:= -lpthread `pkg-config --libs $(PKGLIST)`

kech-atm-o	= main.o draw.o logo.o menu.o

all: kech-atm

clean:
	[ -f kech-atm ] && echo -e "  RM      kech-atm" || true
	rm -f kech-atm
	find . -type f -name '*.o' -delete -exec sh -c "echo '  RM      {}'" \;

%.o: %.png
	echo -e "  LD      $@"
	$(LD) -s -r -b binary -o $@ $<

%.o: %.c
	echo -e "  CC      $@"
	$(CC) $(CFLAGS) -c $< -o $@

kech-atm: $(kech-atm-o)
	echo -e "  CC      $@"
	$(CC) -o $@ $(kech-atm-o) $(LDFLAGS)
