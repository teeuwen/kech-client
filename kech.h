/*
 *
 * kech-client
 * kech.h
 *
 * Copyright (C) 2018 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef _KECH_H
#define _KECH_H

#include <pthread.h>
#include <stdbool.h>
#include <stdint.h>

#define KEY_BUFFER	7
#define AMOUNT_LENGTH	6
#define PIN_LENGTH	6

#define KEY_ENTER	100

#define BANNER_WIDTH	1024
#define BANNER_HEIGHT	420

#define PADDING_TOP	20
#define PADDING_BOTTOM	20

#define BUTTON_MAX	8
#define BUTTON_WIDTH	150
#define BUTTON_HEIGHT	75
#define BUTTON_PADDING	20

#define BULLET_RADIUS	40
#define BULLET_PADDING	40

#define TEXTBOX_HEIGHT	(BULLET_PADDING * 2 + BULLET_RADIUS)
#define TEXTBOX_WIDTH	(BULLET_PADDING + (BULLET_RADIUS * 2 + BULLET_PADDING) \
			* KEY_BUFFER)
#define PINBOX_WIDTH	(BULLET_PADDING + (BULLET_RADIUS * 2 + BULLET_PADDING) \
			* PIN_LENGTH)

enum {
	ALIGN_LEFT,
	ALIGN_CENTER,
	ALIGN_RIGHT
};

enum {
	_NET_WM_NAME,
	_NET_WM_STATE,
	_NET_WM_STATE_FULLSCREEN,
	ATOM_MAX
};

extern char qbuf[], buf[];
extern unsigned char bufi;

int query(uint8_t type, char *data, int32_t n);

extern uint16_t w, h;

void buttons_check(bool press, int x, int y);
void draw_menu(void);
int need_input(int n);

void clear_keybuf(void);
void draw_banner(void);
void draw_background(void);
void draw_text(int x, int y, int _w, int _h, const char *text, bool dark,
		char align, int size);
void draw_button(int n, int t, bool side, bool pressed, const char *text);
void draw_account(int n, int t, bool pressed, char *iban, uint8_t type,
		int64_t balance);
void draw_ibanenter(void);
void draw_amount(void);
void draw_pin(void);
void draw_force(void);
void draw(void);
void draw_fini(void);

#endif
